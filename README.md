[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687153.svg)](https://doi.org/10.5281/zenodo.4687153)

# Electricity generation data



## Repository structure
```
data                    -- containts the dataset in CSV format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```

## Documentation

This datasets shows the annual electricity generation mix for EU28 on country level (NUTS0). 

The ENTSO-E power statistics platform [1] provides the annual generation mix for each MS per energy carrier. The monthly electricity generation by source has been downloaded for all countries included in the Hotmaps toolbox. It should be noted that the electricity generation data per country are only an indicator for the primary energy demand induced by consumption of electricity within a country as imports and exports are not considered in this indicator. Furthermore, the dataset shows electricity output by source and not the primary energy carriers used to generate electricity. The generation mix included in the dataset is based on generation data for the year 2015.

The data will be used in the Hotmaps toolbox to estimate the environmental impact of electrical heating systems.

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.8 page 136ff.

Please note that generation data after the year 2015 is available at [2]

### Limitations of the datasets

The data sets provided have to be interpreted as simplified indicators to link the analysis that can be carried out within the Hotmaps toolbox with the electricity sector. It should be noted that the toolbox focuses on H&C planning and not on electricity system analysis for which the usage of more detailed data is required. The following main limitations have to be considered when interpreting the results:
Data on the electricity generation mix is only provided on an annual basis. The generation mix of individual hours can deviate significantly from the annual average mix. It also has to be noted that import and export of electricity is not considered and the data only provides information on the generation mix within country boarders.




### References
[1] [ENTSO-E - European Network of Transmission System Operators for Electricity](https://www.entsoe.eu/db-query/production/monthly-production-for-all-countries)
[2] [ENTSO-E - Power statistics](https://www.entsoe.eu/data/power-stats/)


## How to cite


Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) 


## Authors
Michael Hartner <sup>*</sup>,

<sup>*</sup> [TU Wien, EEG](https://eeg.tuwien.ac.at/)
Institute of Energy Systems and Electrical Drives
Gusshausstrasse 27-29/370
1040 Wien


## License
For Licensing we refer to the terms of use of the [ENTSO-E Transparency Platform](https://transparency.entsoe.eu/content/static_content/Static%20content/terms%20and%20conditions/terms%20and%20conditions.html)  


## Acknowledgement
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.